package com.zigles.ccodes;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

public class CAbout extends Activity implements OnClickListener {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        TextView tv = (TextView) findViewById(R.id.txtAbout);
        tv.setOnClickListener(this);
    }
    
    public void onClick(View v) {
    	finish();
    }
    
    public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "5669ZLVCH5Z8DAE1KGNR");
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
