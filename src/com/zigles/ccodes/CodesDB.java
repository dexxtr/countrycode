package com.zigles.ccodes;

import static com.zigles.ccodes.DBconstants.DB_NAME;
import static com.zigles.ccodes.DBconstants.DB_PATH;
import static com.zigles.ccodes.DBconstants.DB_VERSION;
import static com.zigles.ccodes.DBconstants.TABLE_NAME;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CodesDB {
	private final Context myContext;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;

	public CodesDB(Context ctx) {
		String fName = DB_PATH + DB_NAME;
		Log.i("CountryCode", "Delete old DB - Start...");
		File f = new File(fName);
		if(f.exists()) {
			try {
				f.delete();
			} catch (Exception e) {
				Log.e("CountryCodes", "Delete: deletion failed!");
				Log.e("CountryCodes", e.getMessage());
			}
		}
		Log.i("CountryCode", "Delete old DB - End!");

		this.myContext = ctx;
		dbHelper = new DatabaseHelper(myContext, true);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		private final Context myCntx;
		public DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
			this.myCntx = context;
		}

		public DatabaseHelper(Context context, boolean copyDatabase) {
			this(context);
			if (copyDatabase) {
				Log.i("CountryCodes", "Copy DB - Start...");
				copyDatabaseFile();
				Log.i("CountryCodes", "Copy DB - End!");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}

		public void copyDatabaseFile() {
			InputStream myInput = null;
			OutputStream myOutput = null;
			SQLiteDatabase database = null;
			
			if (!checkDataBaseExistence()) {
				database = this.getReadableDatabase();
				try {
					myInput = myCntx.getAssets().open(DB_NAME);
					String outFileName = DB_PATH + DB_NAME;
					myOutput = new FileOutputStream(outFileName);

					byte[] buffer = new byte[1024];
					int length;
					while ((length = myInput.read(buffer)) > 0) {
						myOutput.write(buffer, 0, length);
					}
				} catch (FileNotFoundException e) {
					Log.e("CountryCodes", "Source file not found in ASSETS folder...");
					Log.e("CountryCodes", e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					Log.e("CountryCodes", "Input/Output Error");
					Log.e("CountryCodes", e.getMessage());
					e.printStackTrace();
				} finally {
					try {
						if( myOutput != null ) {
							myOutput.flush();
							myOutput.close();
						}
						if( myInput != null )
							myInput.close();
						if (database != null && database.isOpen()) {
							database.close();
						}
					} catch (Exception e) {
						Log.e("CountryCodes", "Unknown Exception");
						Log.e("CountryCodes", e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}

		private boolean checkDataBaseExistence() {
			SQLiteDatabase dbToBeVerified = null;
			try {
				String dbPath = DB_PATH + DB_NAME;
				dbToBeVerified = SQLiteDatabase.openDatabase(dbPath, null, 
						SQLiteDatabase.OPEN_READONLY |
						SQLiteDatabase.NO_LOCALIZED_COLLATORS);	
																
			} catch (SQLiteException e) {
			}

			if (dbToBeVerified != null) {
				dbToBeVerified.close();
			}
			return dbToBeVerified != null ? true : false;
		}
	}
	public CodesDB dbOpen() throws SQLException {
		db = dbHelper.getReadableDatabase();
		return this;
	}
	
	public boolean isOpen() {
		return db.isOpen();
	}
	
	public void dbClose() {
		dbHelper.close();
	}
	
	public void dbRemove() {
		String fName = DB_PATH + DB_NAME;
		Log.i("CountryCode", "Delete old DB - Start...");
		File f = new File(fName);
		if(f.exists()) {
			try {
				f.delete();
			} catch (Exception e) {
				Log.e("CountryCodes", "Delete: deletion failed!");
				Log.e("CountryCodes", e.getMessage());
			}
		}
		Log.i("CountryCode", "Delete old DB - End!");
	}
	
	public Cursor getResult(String strSQL) {
		Cursor cursor = db.rawQuery(strSQL, null);
		return cursor;
	}
	
	public int getCount() {
		String sql = "SELECT * FROM " + TABLE_NAME;
		Cursor cursor = db.rawQuery(sql, null);
		return cursor.getCount();
	}
}