package com.zigles.ccodes;

import static com.zigles.ccodes.DBconstants.COUNTRY_NAME;
import static com.zigles.ccodes.DBconstants.IANA;
import static com.zigles.ccodes.DBconstants.IOC;
import static com.zigles.ccodes.DBconstants.ISO2;
import static com.zigles.ccodes.DBconstants.ISO3;
import static com.zigles.ccodes.DBconstants.ITU;
import static com.zigles.ccodes.DBconstants.TABLE_NAME;
import static com.zigles.ccodes.DBconstants.UN;
import static com.zigles.ccodes.DBconstants.UNISO;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.ads.*;
import com.zigles.android.expll.Launcher;

public class CountryCodes extends ListActivity  {
	private Context context;
	private EditText txtSearch;			
	private Button btnSearch;			
	private TextView txtFound;			
	private ListView lstResult;			
	private CodesDB cDB;				
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        txtFound = (TextView)this.findViewById(R.id.txtFound);
        lstResult = (ListView)this.findViewById(android.R.id.list);
        txtSearch = (EditText)this.findViewById(R.id.txtSearch);
        btnSearch = (Button)this.findViewById(R.id.btnSearch);
        context = getApplicationContext();
        
        AdView adView = (AdView)this.findViewById(R.id.mainAdMob);
        adView.loadAd(new AdRequest());
        
        cDB = new CodesDB(this);

        btnSearch.setEnabled(false);
        txtFound.setText("");
        txtSearch.setText("Name or Code");
        txtSearch.requestFocus();
        txtSearch.selectAll();
   		btnSearch.setEnabled(false);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            	CharSequence text = s.toString();
            	if(text.length() == 0) {
            		btnSearch.setEnabled(false);
            	} else {
            		btnSearch.setEnabled(false);
            		boolean b1 = true;
            		for(int i=0; i<text.length(); i++) {
            			if(text.charAt(i) == (char)34 || text.charAt(i) == (char)39)
            				b1 = false;
            		}
            		if(b1==true)
            			btnSearch.setEnabled(true);
            	}
            }	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
        });
        
        txtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus) {
					txtSearch.setText("");
				}
			}
		});

        txtSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtSearch.setText("");
			}
        });
        
        btnSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		    	context = getApplicationContext();
		    	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		    	imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
	    		cDB.dbOpen();
	    		String SQL = "";
	    		if( txtSearch.getText().charAt(0) >= '0' &&
	    			txtSearch.getText().charAt(0) <= '9' ) {
		    		SQL = "SELECT * FROM " + TABLE_NAME + 
    				" WHERE UNISO " +
    				" MATCH '" + txtSearch.getText() + "'" +
    				" UNION " + 
		    		"SELECT * FROM " + TABLE_NAME + 
    				" WHERE ITU " +
    				" MATCH '" + txtSearch.getText() + "'";
	    		} else {
	    			SQL = "SELECT * FROM " + TABLE_NAME + 
    				" WHERE " + TABLE_NAME +
    				" MATCH '" + txtSearch.getText() + "'";
	    		}
	    		Cursor cursor = cDB.getResult(SQL);
	    		
	    		startManagingCursor(cursor);
	    		String[] columns = new String[] { 
	    				COUNTRY_NAME, 
	    				ISO2, 
	    				ISO3, 
	    				IANA, 
	    				UN, 
	    				IOC, 
	    				UNISO, 
	    				ITU }; 
	    		int[] to = new int[] { 
	    				R.id.txtName, 
	    				R.id.txtISO2, 
	    				R.id.txtISO3,
	    				R.id.txtIANA,
	    				R.id.txtUN,
	    				R.id.txtIOC, 
	    				R.id.txtUNISO,
	    				R.id.txtITU };
	    		
	    		SimpleCursorAdapter mAdapter = 
	    			new SimpleCursorAdapter(context, R.layout.list_item, cursor, columns, to);
	    		setListAdapter(mAdapter);
	    		
	    		Integer recTotal = cDB.getCount();					
	    		Integer recFound = cursor.getCount();				
	    		txtFound.setText("Find: " + txtSearch.getText() + "  Found: " + recFound.toString() + 
	    				"/" + recTotal.toString() + " record(s)");
	    		cDB.dbClose();
	    		lstResult.requestFocus();
			}
		});
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
   		cDB.dbRemove();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu optmenu) {
 	   //super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optmenu, optmenu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.menu_poll:
        	Launcher exPllLauncher = new Launcher(this.getApplicationContext());
        	exPllLauncher.run();
        	return true;
        case R.id.menu_donate:
        	Intent i = new Intent(Intent.ACTION_VIEW, 
   		       Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business" + 
   		    		   "=N3NWXCJMEN5RW&lc=UA&item_name=Alexander&currency_code=USD&" +
   		    		   "bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted"));
        	startActivity(i);        	
        	return true;
        case R.id.menu_help:
        	startActivity(new Intent(this, CHelp.class));
        	return true;
        
        case R.id.menu_about:
        	startActivity(new Intent(this, CAbout.class));
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "5669ZLVCH5Z8DAE1KGNR");
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
