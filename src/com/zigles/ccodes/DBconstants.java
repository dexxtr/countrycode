package com.zigles.ccodes;

import android.provider.BaseColumns;

public interface DBconstants extends BaseColumns {
	public static String DB_PATH = "/data/data/com.zigles.ccodes/databases/";
	public static String DB_NAME = "ccodes.db3";
	public static final int DB_VERSION = 1;
	public static final String TABLE_NAME = "vCCodes";
	public static final String TAG = "CountryCodes";

	public static final String COUNTRY_NAME = "CountryName";
	public static final String ISO2 = "ISO2";
	public static final String ISO3 = "ISO3";
	public static final String IANA = "IANA";
	public static final String UN = "UN";
	public static final String IOC = "IOC";
	public static final String UNISO = "UNISO";
	public static final String ITU = "ITU";
}