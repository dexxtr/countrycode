package com.zigles.ccodes;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.flurry.android.FlurryAgent;

public class Loader extends Activity {

	public static final long TIME_SHOW_LOADER = 2000L;

	private CodesDB cDB;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loader);
		final Intent intent = new Intent(this, CountryCodes.class);
		
		final Handler speedHandler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				startActivity(intent);
				finish();
			}
		};
		speedHandler.postDelayed(runnable, 2000);
		new AsyncLoader().execute(cDB);
	}
	
	public void onStart() {
	   super.onStart();
	   FlurryAgent.onStartSession(this, "5669ZLVCH5Z8DAE1KGNR");
	}
	
	public void onStop() {
	   super.onStop();
	   FlurryAgent.onEndSession(this);
	}

	private class AsyncLoader extends AsyncTask<CodesDB, Void, Void> {
    	@Override
    	protected Void doInBackground(CodesDB... params) {
    		long oldTime = System.currentTimeMillis();

    		cDB = new CodesDB(Loader.this);

    		while( cDB == null || 
    			(System.currentTimeMillis() - oldTime) < TIME_SHOW_LOADER )
        		try {
    				Thread.sleep(100);
    			} catch (InterruptedException e) {
    				Log.e("CountryCodes", "AsyncLoader.doInBackground() error: TimeoutException"); 
    				e.printStackTrace();
    			}
    		return null;
    	}
    }
}